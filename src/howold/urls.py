from django.conf.urls.defaults import patterns, url

urlpatterns = patterns('',
                       
    url(r'^$', 'howold.age.views.home', name='home'),
    url(r'^age/$', 'howold.age.views.age', name='age'),
    url(r'^error/$', 'howold.age.views.error', name='error'),
)
