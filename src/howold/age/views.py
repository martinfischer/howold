# coding=utf8
from django.shortcuts import render_to_response, redirect
from datetime import date
from django import forms
from django.core.context_processors import csrf
import calendar

class DatumsFormular(forms.Form):    
    tage = range(1, 32)
    tag_zip = zip(tage, tage)
    day = forms.ChoiceField(tag_zip)

    monate = ((1,"January"),
              (2,"February"),
              (3,"March"),
              (4,"April"),
              (5,"May"),
              (6,"June"),
              (7,"July"),
              (8,"August"), 
              (9,"September"), 
              (10,"October"),
              (11,"November"),
              (12,"December"))
    month = forms.ChoiceField(monate)
    
    jahre = range(1900, date.today().year)
    jahr_zip = zip(jahre, jahre)
    year = forms.ChoiceField(jahr_zip)
    
def home(request):
    form = DatumsFormular(initial={"year":1978})
    mydict = {"form": form,}
    mydict.update(csrf(request))
    return render_to_response("home.html", mydict)

def age(request):
    if request.method == "POST":
        form = DatumsFormular(request.POST)
        
        if form.is_valid():
            tag = form.cleaned_data["day"]
            monat = form.cleaned_data["month"]
            jahr = form.cleaned_data["year"]
            
            try:
                geb_datum = date(int(jahr), int(monat), int(tag))
            except ValueError:
                return redirect("error")

            heute = date.today()
            
            if calendar.isleap(geb_datum.year) and geb_datum.day == 29 and geb_datum.month == 2:
                if calendar.isleap(heute.year+1):
                    dieses_jahr = date(heute.year+1, geb_datum.month, geb_datum.day)
                elif calendar.isleap(heute.year+2):
                    dieses_jahr = date(heute.year+2, geb_datum.month, geb_datum.day)
                elif calendar.isleap(heute.year+3):
                    dieses_jahr = date(heute.year+3, geb_datum.month, geb_datum.day)
                else:
                    dieses_jahr = date(heute.year, geb_datum.month, geb_datum.day)
                    
                alles_gute = False
                if dieses_jahr == heute:
                    alles_gute = True
                    
                if dieses_jahr <= heute:
                    alter = (heute.year - geb_datum.year) / 4
                else:
                    alter = ((heute.year - geb_datum.year) - 1) / 4               
                    
            else:
                dieses_jahr = date(heute.year, geb_datum.month, geb_datum.day)
            
                alles_gute = False
                if dieses_jahr == heute:
                    alles_gute = True
                    
                if dieses_jahr <= heute:
                    alter = heute.year - geb_datum.year
                else:
                    alter = (heute.year - geb_datum.year) - 1
            
            mydict = {"geb_datum":geb_datum, 
                      "heute":heute, 
                      "dieses_jahr":dieses_jahr,
                      "alles_gute": alles_gute,
                      "alter":alter,
            }
            
            mydict.update(csrf(request))
            return render_to_response("age.html", mydict)

    else:
        form = DatumsFormular(initial={"year":1978})

    return redirect("home")

def error(request):
    message = "This date doesn't exist"
    form = DatumsFormular(initial={"year":1978})
    mydict = {"form":form, "message":message}
    mydict.update(csrf(request))
    return render_to_response("error.html", mydict)
